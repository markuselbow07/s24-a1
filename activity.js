db.users.insertOne({
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "contact": {
            "phone": "09196543410",
            "email": "janedoe@mail.com"            
            },
         "courses": ["CSS", "Javascript", "python"],
         "department": "HR"    
    });


db.users.insertMany([{
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "contact": {
            "phone": "09179876543",
            "email": "stephenhawking@mail.com"            
            },
         "courses": ["React", "PHP", "python"],
         "department": "HR"    
    }, 
    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "contact": {
            "phone": "09061234567",
            "email": "neilarmstrong@mail.com"            
            },
         "courses": ["React", "Laravel", "SASS"],
         "department": "HR"    
    }
    ]);

db.users.find({ $or [{ firstName: "s"}, { lastName: "d"}]});


db.users.find(
	{$in "firstName": {"Jane", "Stephen", "Neil"}}, 
	{"_id": 0, "contact": 0, "department": 0}
);

db.users.find(
	{$in "lastName": {"Doe", "Hawking", "Armstrong"}}, 
	{"_id": 0, "contact": 0, "department": 0}
);
db.users.find({"age": {$gte: 70}}, { "department": {$and: "HR"}});

db.users.find({"age": {$lte: 70}}, { "lastName": {$and: "e"}});